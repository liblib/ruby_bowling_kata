# Author Lidlanca
# Date 3/26/2015

class AsciiCanvas
    
    def initialize(width,height,char="-")
        @default_char = char
        @buf_width  = width
        @buf_height = height
        @x_padding = 1
        @y_padding = 1
        @allow_y_expansion = true
        reset_paint_position()
        @buffer = Array.new(height) { Array.new(width) { char } }
    end 
    
    def reset_paint_position
        @max_x = 0  # maintain the max X for the most recent block draw
        @max_y = 0  # maintain the max Y for the most recent block draw
        @cur_x = 0  # the current cursor X position 
        @cur_y = 0  # the current cursor Y position 
    end 
    
    # provide some information about the block of text.
    # such as width and height.
    def text_block_size(text)
        lines = text.split("\n")
        height = text.lines.size
        width = lines.max_by(&:length).size
        height = lines.size
        return {:width=>width,:height=>height,:lines=>lines}
        # return  [width,height,lines]
    end
    
    # draw a block of text starting from the provided cursor position (x,y)
    def draw_text_at(text,x=0,y=0)
        canvas = @buffer
        cur_x = x
        cur_y = y
        text.lines.each {|line|
             if cur_y > canvas.size-1
                    if @allow_y_expansion # we flag is on, the canvas will allocate new lines.
                        (cur_y+1-canvas.size).times {
                            @buffer << Array.new(@buf_width) { @default_char }     
                            @buf_height +=1
                        }
                    else
                        puts "Warning:painting outside of canvas Y buffer"    
                        break
                    end 
                end 
                    
            line.chomp.split("").each {|letter|
                canvas[cur_y][cur_x] = letter
                cur_x += 1
            }
            cur_x=x
            cur_y+=1
        }
    end
    
    # Draw block of text, and manage the next start coordinates for the next block draw.
    # if block is overflow-x it will be moved to the next line
    def draw_next_block(text)
       canvas = @buffer
       block_info  = text_block_size(text)
       @max_y = [@max_y, block_info[:height]].max
       
       # if new block will be drawn outside the canvas move it to next line
       if block_info[:width] + @cur_x + @x_padding > @buf_width-1  
            # puts "NEW LINE"
            draw_new_line()
       end
       draw_text_at(text, @cur_x, @cur_y)
       @max_x = block_info[:width] + @x_padding
       @cur_x = @cur_x +  @max_x
    end 
    
    # move cursor to new line
    def draw_new_line(allow_multi=false)
        @cur_y = @cur_y + @max_y + @y_padding 
        @cur_y += 1 if allow_multi
        if @cur_y > @buf_height-1
            if @allow_y_expansion
                        (1).times {
                            @buffer << Array.new(@buf_width) { @default_char }     
                            @buf_height +=1
                        }
            else 
                puts "Warning: trying to place a new line out side the Y buffer"            
            end
        end
        @max_y = 0
        @max_x = 0
        @cur_x = 0
    end 

    def print_buffer
        canvas = @buffer 
        canvas.each {|line|
            puts line.join("")
        }
    end 
end
