#Advanced developers: Uncomment the following to understand more about the RSpec "focus" filter
#require File.expand_path(File.dirname(__FILE__) + '/spec_helper')

require 'bowling_game'

# The top of your testing block
# More examples of tests can be found here: https://github.com/rspec/rspec-expectations
RSpec.describe BowlingGame, "#score" do

  let(:game) { BowlingGame.new }

  it "knows the score is 0 for a new game" do
    expect(game.score).to eql(0)
  end

  it "expect the score to be 0 for gutter ball on first roll" do
  	game.roll(0)
  	expect(game.score).to eql(0)
  end

	it "expect the 1st roll score to be 10 for 10 pins " do
	game.roll(10)
	expect(game.score).to eql(10);
	end

  it "expect the 1st roll score to be 10 for 10 pins " do
  	game.roll(10)
  	expect(game.score).to eql(10);
  end

  it "is game.1st frame spare when first roll 0, 2nd roll 10" do
    game.roll(0)
    game.roll(10)
    expect(game.frame(0).is_spare?).to eql(true)
  end

  # more tests go here
end






# Frame validation

# GameFrame - Regualr frame
    #maximum score(total pins) of frame must be smaller or equal to 10
    # expect a strike game to have a score when the next frame is completed
    
    # if a frame is a strike,  followed by another strike frame, 
        # Expect the frame to have a score when 
            # If next_frame is not last AND next_next_frame has 1st roll points
            # OR
            # If the next_frame is the last frame 
                #Expect frame to have a score when
                    # next_frame(also last frame) and has the first 2 rolls points

    # Example 
    #      8    9   10        <--- frame number
    #    |10| |10| |10,5,3|   <--- frame pins
    #     ^----------------------- frame 
    #          ^--------------------- next_frame
    #                ^------------------ next_next_frame (also last frame)

    # case 1: calculating frame(8) 
    #           is strike? True |10| 
    #           next_frame is strike?  True |10|
    #           next_next_frame has first roll? True (10)  |10,5,3|
    #         Calculate points 
    #         frame score = 10 + ({{next_next_frame.first_roll_score}})

    # case 2: calculating frame(9) 
    #         is a strike? True |10|
    #         next_frame is a strike but also the last frame  (There is no Next_next_frame)
    #         Last frame has 2 first rolls? True (10,5)
    #         Calculate points 
    #         frame(9) score: (10) + ({{next_frame.first_roll_Score}}+{{next_frame.second_roll_Score}})

    # Note on calculating last frame:
    # last frame score is consist of the sum of the actual pins in each roll.
    # For example if the last frame scores is | 10, 10, 10| ( 3 strikes )
    # The score of the last frame is simply 10+10+10 



# LastGameFrame  - Special last frame
    # if first or second roll result in a strike or spare 
        # frame should not be completed and a 3rd roll should be possible

    # if first roll is not a strike ( < 10)
        # 2nd roll score(pins) must be smaller or equal to (10-first_roll_pins)
    # if first roll is a strike 
        # 2nd roll (score) <= 10

    # if first and second roll result in an ":open" frame (a completed frame with no strike nor spare)
        #3rd roll is not possible and frame is completed

    # if first and second roll is a strike 
        # Max possible points(pins) is 30
        # Each frame roll points must be <= 10

    # is_last? should be true and frame should be the last frame in the array.

    # is_spare? should be true when 1st and 2nd roll produce a spare

    # is_strike? should be true when 1st roll produce a strike 
        # note:
        # even though last frame can have more then 1 strike. we only care about the first strike for the logic of the scoring
        # we use it to check if frame will allow a 3rd roll

