require 'pp'
require_relative "ascii_canvas.rb"

# Online Bowling Calculator helper http://www.bowlinggenius.com/

class GameFrame
    def initialize ind, frames
        @MAX_ROLLS = 2            # maximum allowed rolls for this frame type
        @frames = frames         # Reference to Game frames array
        @frame_index = ind         # The index of this frame in the @frames array
        @frames_roll_pins = []     # Pins per roll array
        @roll_ind = 0             # Current roll index
        @frame_score = nil
    end

    def score 
        @score
    end 

    # helper to sum an array
    def sum array
        array.inject(:+)
    end


    def roll(pins)
        @frames_roll_pins << pins
        @roll_ind += 1
    end


    # return the state of the frame
    # :spare, :strike, :open imply frame is completed
    # :nil imply frame is not completed
    def frame_state 
        if is_spare?
            :spare
        elsif is_strike?
            :strike
        elsif @roll_ind == @MAX_ROLLS
            :open  # The frame is completed, in bowling jargon open mean a frame that did not had strike or spare.
        else
            nil
        end
    end


    # return a string of a frame as a pretty print, yet using ugly code :)
    # some complixity is due to different printing for last frame.
    # Example of a frame output
    #  |-------------|
    #  |      1      |   <-- frame id
    #  |-------------|
    #  |    5 |    / |   <-- roll score
    #  |      -------|
    #  |     20      |   <-- frame score
    #  |-------------|
    # 
    def pretty_print_frame
        buf = ""
        frame_score = calculate_frame_score()
        frame_score = "" if frame_score.nil? 
         buf += ("\n|" + "-".ljust(13,"-") + "|\n")
         buf += ( sprintf("|%s|\n",(@frame_index+1).to_s.center(13) ) )
         buf += ( "|" + "-".ljust(13,"-") + "|\n" )

        if is_last?
            last_frame_roll_score = [get_roll_score(0),get_roll_score(1),get_roll_score(2)]
            if is_spare?
                last_frame_roll_score[1] = "/"
            else
                if ready_for :spare
                    last_frame_roll_score[2] = "/" if last_frame_roll_score[2]+last_frame_roll_score[1] == 10 # 10, 5, 5 => X | 5 | /
                end 
                last_frame_roll_score[0] = "X" if last_frame_roll_score[0] == 10
                last_frame_roll_score[1] = "X" if last_frame_roll_score[1] == 10
                last_frame_roll_score[2] = "X" if last_frame_roll_score[2] == 10

            end
             buf +=  ( sprintf("|%2s |%3s |%3s |\n",*last_frame_roll_score) )
        else
            if is_strike?
                 buf+=  ( sprintf("|%5s |%5s |\n","","X") )
            elsif is_spare?
                 buf+=  ( sprintf("|%5s |%5s |\n",get_roll_score(0),"/") )
            else     
                 buf+=  ( sprintf("|%5s |%5s |\n",get_roll_score(0),get_roll_score(1)) )
            end
        end
         buf += ( "|" + "      ".ljust(13,"-") + "|\n" )
         buf += ( "|" +  calculate_frame_score().to_s().center(13," ") + "|\n" )
         buf += ( "|" + "-".ljust(13,"-") + "|\n" )
         # puts buf
         return buf
    end 

    #return the current roll index
    def get_current_roll_index
        @roll_ind
    end


    # get score for a roll index. if roll_ind is nil return the entire array
    def get_roll_score(roll_ind=nil)
        if roll_ind.nil?
            @frames_roll_pins 
        else
            @frames_roll_pins[roll_ind]
        end
    end


    # quick method to get the sum of all the available rolls in the frame
    def frame_score_sum
        return sum(get_roll_score)
    end


    # Get the previous frame and perform score calculation(prev_frame.calculate_frame_score())
    # return 0 if frame does not have calculable score or if previous frame does not exists.
    def prev_frame_calculated_score
        pframe = self.get_prev_frame()
        if !pframe.nil?
            pframe_calc_score = pframe.calculate_frame_score()
            pframe_calc_score = pframe_calc_score.nil? ? 0 : pframe_calc_score
        else 
            pframe_calc_score = 0
        end 
    end 


    # messy and long but it does the job
    # return frame calculated score
    # or nil is not yet calculable 
    def calculate_frame_score()

        state = self.frame_state() 
        next_frame = self.get_next_frame()
        pframe = self.get_prev_frame()
        pframe_calc_score = prev_frame_calculated_score()
        
        # if current frame is :spare and the next frame is "ready_for" spare 
        @score = if state == :spare && !next_frame.nil? && next_frame.get_current_roll_index > 0
            pframe_calc_score + self.frame_score_sum() + next_frame.get_roll_score(0)


        elsif state == :strike && !next_frame.nil?
            # when we have 2 sequential strikes, we depend on the self->next->next,  first roll score.
            if next_frame.is_strike? # 2nd strike
                next_next_frame = next_frame.get_next_frame()  # get the  self->next->next frame
                if !next_next_frame.nil? && next_next_frame.get_current_roll_index > 0 # check if the next next frame has at least one played roll
                    #self.frame_score_sum() + next_frame.frame_score_sum() + next_next_frame.get_roll_score(0)
                    pframe_calc_score + 10 + 10 + next_next_frame.get_roll_score(0)
                else
                    # this is an "edge" case that handle when we don't have a "self->next->next" frame
                    # because the self->next frame is probably the last frame.
                    # if it is the last frame we need to check if "ready_for :strike" 
                    # and return the next frame 1st and 2nd roll points. 
                    if next_frame.is_last?
                        # TODO add ready_for :spare and test, we need at least 2 rolls in the next frame 
                        if next_frame.ready_for :spare
                            # puts next_frame.get_roll_score(1)
                            pframe_calc_score + 10 + next_frame.get_roll_score(0) + next_frame.get_roll_score(1)
                        end
                    end
                end 
            else  # current frame is a strike but 2nd frame is not  case
                if !next_frame.frame_state.nil? # make sure frame is completed 
                    pframe_calc_score + 10 + (next_frame.get_roll_score(0) + next_frame.get_roll_score(1))
                end 
            end

        elsif state == :open  # case frame is completed :open indicate there is no strike nor spare
            if !pframe.nil? && !pframe.frame_state.nil? # check that we have a previous frame and that the  frame is completed
                pframe_calc_score + self.frame_score_sum() 
            else  # TODO this is redundent we can probably avoid this else portion
                pframe_calc_score + self.frame_score_sum()
            end 
        else  # Case: score can not be calculated yet.  
            nil#nil score can not be calculated it is not completed yet
        end
    end


    # Get a reference to the previous frame if exists or nil otherwise.
    def get_prev_frame()
        if @frame_index > 0
            return @frames[@frame_index-1]
        else 
            return nil
        end 
    end


    # Get a reference to the next frame if exists or nil otherwise
    def get_next_frame()
        if !is_last?
            @frames[@frame_index+1] 
        else 
            return nil
        end 
    end


    # An helper used to determain if the frame has the minimum rolls required to check for spare or strike
    # mode_sym accept   :spare or :strike
    # for :spare mode   ready if at least the first 2 rolls were played
    # for :strike mode  ready if at least the 1st roll was played 

    def ready_for(mode_sym)
        if mode_sym == :spare
             return @frames_roll_pins.size > 1
        elsif mode_sym == :strike
            return @frames_roll_pins.size > 0
        end
    end


    # check if the frame is the last frame
    def is_last?
        @frame_index == 10-1
    end
    def is_first? 
        @frame_index == 0
    end


    # check if the frame is a spare 
    # it only check 1st and 2nd roll important to know when used in LastGameFrame
    def is_spare?
        ready_for(:spare) && get_roll_score(0)+get_roll_score(1) == 10 && get_roll_score(0) < 10
    end


    # check if the frame is a strike
    def is_strike?
        ready_for(:strike) && get_roll_score(0) == 10
    end
end


# Extedns GameFrame
# The last frame of the game has some different behavior
# When calculating the score, and when checking for the state of the frame.
# The last frame allows more up to 3 rolls, if the 1st or 2nd roll resulted in spare or strike
class LastGameFrame < GameFrame
    # def initialize ind, frames
    # ...
    # end


    def frame_state
        # In the last frame, if the first 2 rolls produce strike or a spare. 
        # The frame is not completed yet and additional roll is given.
        # we use the logic from the parent class GameFrame.frame_state
        state = super 

        if [:strike,:spare].include? state # frame is either strike or spare
            if @frames_roll_pins.size < 3  # and is not completed 
                return nil # return nil to indicate frame is not completed. 
            else
                return :the_end
            end 
        else 
            return state
        end 
    end 


    def calculate_frame_score 
        state = frame_state
        @score = if !state.nil? || state == :the_end
            prev_frame_calculated_score() + frame_score_sum()
        else 
            nil
        end
    end


    # assume this is the last frame just return true
    def is_last?
        true
    end

end




class BowlingGame
  def initialize
      @current_frame = 0
      @game_score = 0
      @frames = []

      prepare_frames()
  end


  # return the active frame of the game
  def get_active_frame
      @frames[@current_frame]
  end 

  def reset_game
      initialize
  end

  # prepare and initialize the frames of the game.
  def prepare_frames
      @frames = []

      # the first 9 frames are regular frames (GameFrame) 
      9.times do |i|
          @frames << GameFrame.new(i,@frames) # pass frame its index and the frames array
      end

      # The last frame is of type LastGameFrame.
      @frames << LastGameFrame.new(9,@frames)
      return @frames
  end


  # Advance the pointer of the active frame to the next frame if avilable. 
  def move_pointer_to_next_frame
      if !get_active_frame.is_last?
          @current_frame += 1
      else
          #TODO
          # option 1 sliently ignore CURRENT
          # option 2 raise an error 
              # raise "NO_MORE_NEXT_FRAME"
          # option 3 return a flag and check for it to end game.
      end 
  end 

  def update_game_score
      max_score = 0
      active_frame = get_active_frame()
      active_frame_score = active_frame.calculate_frame_score()

      if active_frame_score.nil?
        prev_frame = active_frame.get_prev_frame()
        if !prev_frame.nil?
            pf_score = prev_frame.calculate_frame_score()         
            prev_prev_frame = prev_frame.get_prev_frame()
            if !prev_prev_frame.nil?
                p_pf_score = prev_prev_frame.calculate_frame_score() 
            end
        end
      end
      @score = [active_frame_score,pf_score,p_pf_score].map(&:to_i).max
      if !active_frame_score.nil? 
          @score = active_frame_score
          # puts "TODO:CURRENT FRAME HAS CALCULATED SCORE DO WE NEED TO CONTINUE?" # PROBABLY RESOLVED
          return @score
      end

      puts "CURRENT GAME SCORE:#{@score} At Frame #{@current_frame} " # For debugging comment out if not needed
  end

  def roll(pins_knocked_down)
      active_frame = get_active_frame()    
      active_frame.roll(pins_knocked_down)
      state = active_frame.frame_state()  #if frame is still active state is nil
      update_game_score()
      if state == nil
          #frame is not completed
      else
          #frame is completed could be strike, spare, open
        move_pointer_to_next_frame()
      end 
  end


  # called to retrieve the current game score
  def score
      @score
    #TODO
    # return the most recent completed frame that has a calculated score.
  end


  # get a frame by an index
  def frame ind
      @frames[ind]
  end
  def frames
      @frames 
  end 


# Helper to draw game board using AsciiCanvas
def draw_game_board 
        canvas = AsciiCanvas.new(100,30," ") # Canvas Width X Height 
        # Print each frame on AsciiCanvas
        frames.each {|frame| canvas.draw_next_block( frame.pretty_print_frame() ) }

canvas.draw_next_block %q{
               _....._
            .;;'      '-.
          .;;: _         '.
         /;;:'(_)          \
        |;;:'_     _        |
        |;;:(_)   (_)       |
        |;;::.              |
         \;;::.            /
          ';;::.         .'
            '-;;:..  _.-'
}
        canvas.print_buffer
    end 

private
  # ...and here when the above becomes too complex.

end




#Inline Testing

game = BowlingGame.new

# Example Game 1 simulation
# Expected 2,6,35,55,71,88,97,115,123,148
def game1_sample(game)
    [
        1,1,
         2,2,
         10,
         10,
         9,1,
         6,4,
         7,2,
         10,
         4,4,
         10,10,5
     ].each { |pins| game.roll pins }
end

# Example Game 2 simulation
# Expected 20,50,80,110,140,170,200,230,260,290
def game2_sample(game)
    [
        5,5,
        10,
        10,
        10,
        10,
        10,
        10,
        10,
        10,
        10,
        10,
        10
    ].each {|pins| game.roll pins}
end

puts "Simulating Game 1"
game1_sample(game) # simulate a game
game.draw_game_board() # draw game result
game.reset_game() # reset game allow us to use the same object for a new game simulation

puts "Simulating Game 2"
game2_sample(game)    # simulate another game
game.draw_game_board() # draw game result
game.reset_game()       # reset game

# game.frames.each {|frame| pp frame.calculate_frame_score()}
puts "-----End-----"
# pp game.inspect
